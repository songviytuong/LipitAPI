<?php
session_start();
error_reporting(E_ALL^E_NOTICE);
error_reporting(E_ERROR);

include 'Lipit_API.php';

$api_id = 8888; // interger
$api_key = "user8888"; // string
$api_token = "pass8888"; // string

// truyen du lieu the
$name = $_POST['name']; // string
$address = $_POST['address']; // string
$ma_bao_mat = $_POST['ma_bao_mat'];

// checm ma bao mat
if($ma_bao_mat != $_SESSION['code_security']) {
     echo json_encode(array('code' => 1, 'msg' => "Sai mã bảo mật. Vui lòng nhập lại"));
     exit();
}

$vippay_api = new Lipit_API();
$vippay_api->setAPI_ID($api_id);
$vippay_api->setAPI_KEY($api_key);
$vippay_api->setAPI_TOKEN($api_token);
$vippay_api->setName($name);
$vippay_api->setAddress($address);
$vippay_api->cardCharging();
$code = $vippay_api->getCode();
$transaction_code = $vippay_api->getTransactionCode();

$msg = "";
if($code === 0){
    $msg = $vippay_api->Response(200);
}else{
    $msg = $vippay_api->Response($code);
}

echo json_encode(
    array(
        'code' => $code,
        'transaction_code' => $transaction_code,
        'msg' => $msg
    )
);
