<?php
class Lipit_API {

    private $api_id;
    private $api_key;
    private $api_token;
    private $code;
    private $msg;
    private $transaction_code;
    private $name;
    private $address;

    /**
     * @return the $name
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return the $address
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * @return the $merchant_id
     */
    public function getAPI_ID() {
        return $this->api_id;
    }

    /**
     * @return the $pin
     */
    public function getAPI_KEY() {
        return $this->api_key;
    }

    /**
     * @return the $seri
     */
    public function getAPI_TOKEN() {
        return $this->api_token;
    }

    /**
     * @return the $code
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @return the $msg
     */
    public function getMsg() {
        return $this->msg;
    }

    /**
     * @return the $transaction_id
     */
    public function getTransactionCode() {
        return $this->transaction_code;
    }

    /**
     * @param field_type $merchant_id
     */
    public function setAPI_ID($api_id) {
        $this->api_id = $api_id;
    }

    /**
     * @param field_type $pin
     */
    public function setAPI_KEY($api_key) {
        $this->api_key = $api_key;
    }

    /**
     * @param field_type $seri
     */
    public function setAPI_TOKEN($api_token) {
        $this->api_token = $api_token;
    }

    /**
     * @param field_type $code
     */
    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * @param field_type $msg
     */
    public function setMsg($msg) {
        $this->msg = $msg;
    }

    /**
     * @param field_type $transaction_id
     */
    public function setTransactionCode($transaction_code) {
        $this->transaction_id = $transaction_code;
    }

    /**
     * @param field_type $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @param field_type $address
     */
    public function setAddress($address) {
        $this->address = $address;
    }

    public function cardCharging() {

        $fields = array(
            'api_id' => $this->api_id,
            'api_key' => $this->api_key,
            'api_token' => $this->api_token,
            'name' => $this->name,
            'address' => $this->address
        );

        $ch = curl_init("http://api-request.dev");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        $this->code = $result->code;
        $this->transaction_code = $result->transaction_code;
    }
    
    public function Response($code){
        $arr = array(
            200 => 'Thành công.',
            1 => 'Không thành công.',
            100 => 'Tham số API đầu vào không đúng.',
            400 => 'Không hiểu'
        );
        if($code){
            return $arr[$code];
        }else{
            return $arr[400];
        }
    }

}
