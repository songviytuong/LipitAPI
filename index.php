<html>
<head>
    <title>Cổng thanh toán trực tuyến qua thẻ cào</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="vippay.vn"/>
    <meta name="description" content="Thanh toán trực tuyến"/>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/bootstrap-responsive.css"/>
    <link rel="stylesheet" href="css/bootstrap-theme.css"/>

    <style type="text/css">
        table tr td {
            border: none !important;
            vertical-align: middle
        }
    </style>

    <!-- Script -->
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            // nap the
            $("#fnapthe").ajaxForm({
                dataType: 'json',
                url: 'ajax/card.php',
                beforeSubmit: function () {
                    $("#loading_napthe").show();
                },
                success: function (data) {
                    if(data.code == 0){
                        $("#msg_napthe").html('<div class="alert alert-success">' + data.msg + ' - Transaction Code: ' + data.transaction_code + '</div>');
                    }else{
                        $("#msg_napthe").html('<div class="alert alert-danger">' + data.msg + '</div>');
                    }
                    $("#loading_napthe").hide();
                    $("#captcha").attr('src', 'captcha/CaptchaSecurityImages.php?' + Math.random());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#msg_napthe").html('<div class="alert alert-danger">Có lỗi trong quá trình thực hiện</div>');
                    $("#loading_napthe").hide();
                    $("#captcha").attr('src', 'captcha/CaptchaSecurityImages.php?' + Math.random());
                }
            });
        });
    </script>
</head>
<body>
<div style="margin: 0 auto; width: 500px; border: 1px solid #ccc; margin-top:30px; border-bottom-left-radius: 20px;">
    <a href="#" class="label label-success"
       style="color: #fff; width: 100%; display: inline-block; font-size: 18px; padding:10px; text-align: center">
        Lipit API REQUEST
    </a>
    <form action="#" method="post" id="fnapthe">
        <table class="table table-responsive">
            <tbody>
                <tr>
                    <td colspan="2">
                        <div id="msg_napthe"></div>
                    </td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" value="" name="name"  class="form-control" autocomplete="off"/></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><input type="text" value="" name="address"  class="form-control" autocomplete="off"/></td>
                </tr>
                <tr>
                    <td>Mã bảo mật:</td>
                    <td>
                        <input type="text" id="ma_bao_mat" name="ma_bao_mat" class="form-control" style="  width: 100px; float: left;  margin-right: 10px;" autocomplete="off"/>
                        <img src="captcha/CaptchaSecurityImages.php?height=28" id="captcha"/>
                        <img src="images/refresh.gif" style="position: relative; left: 12px; top: -1px; cursor: pointer;"
                             onclick="document.getElementById('captcha').src='captcha/CaptchaSecurityImages.php?height=28&'+Math.random();"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input class="btn btn-success" type="submit" value="Nạp thẻ"/>
                        <div id="loading_napthe" style="display: none; float: right"><img src="images/loading.gif"/> &nbsp;Xin
                            mời chờ...
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
</body>
</html>
